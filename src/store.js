import { createStore, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";

import reducer from "./redusers";

const logMiddleware = (store) => (dispatch) => (action) =>  {
    console.log(action.type);
    return dispatch(action);
};

const stringMiddleware = () => (dispatch) => (action) =>  {
    if(typeof action === 'string') {
        return dispatch({
            type: action
        })
    }
    return dispatch(action);
};


const store = createStore(reducer, applyMiddleware(thunkMiddleware, stringMiddleware, logMiddleware));


const delayedActionCreated = (timeout) =>  (dispatch) => {
    setTimeout(() => dispatch({
        type: 'DELAYED_ACTION'
    }), timeout)
};

store.dispatch(delayedActionCreated(3000));

export default store;

