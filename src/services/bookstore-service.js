import React from "react";

export default class BookstoreService  {

    data =  [
        {
            id: 1,
            title: 'Dark Tower',
            author: 'Stephen King',
            price: 110,
            coverImage: 'https://d3525k1ryd2155.cloudfront.net/h/869/808/1103808869.0.m.jpg'
        },
        {
            id: 2,
            title: 'Game of Thrones',
            author: 'George Martin',
            price: 7,
            coverImage: 'https://images-na.ssl-images-amazon.com/images/I/51HoTTx2ZhL._SX327_BO1,204,203,200_.jpg'
        },
        {
            id: 3,
            title: 'Witcher',
            author: 'Andrzej Sapkowski',
            price: 15.29,
            coverImage: 'https://prodimage.images-bn.com/pimages/9780316273718_p0_v2_s550x406.jpg'
        },

    ];

    getBooks() {
        return new Promise(( resolve, reject ) =>  {
            setTimeout(() => {
                if(Math.random() <  0.75) {
                    resolve(this.data)
                } else {
                    reject(new Error('Something bad happened!'))
                }
            }, 700)
        });
    }
}
