import React from "react";
import {BooksStoreServiceConsumer} from "../BookStoreServiceContext";

const withBookstoreService = () => (Wrapped) => {
    return (props) => {
        return (
            <BooksStoreServiceConsumer>
                {
                    (bookStoreService) => {
                       return (<Wrapped {...props}
                                        bookStoreService={bookStoreService}/>);
                    }
                }
            </BooksStoreServiceConsumer>
        )
    }
};

export default withBookstoreService;
