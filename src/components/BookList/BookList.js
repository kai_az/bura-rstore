import React, {Component} from "react";
import './BookList.css'
import { bindActionCreators } from 'redux';
import BookListItem from "../BookListItem";
import {connect} from "react-redux";
import withBookstoreService from "../hoc/with-bookstore-service";
import { fetchBooks, bookAddedToCart } from "../../actions";
import {compose} from "redux";
import Spinner from "../Spinner";
import ErrorIndicator from "../ErrorIndicator";


const BookList = ({ books, onAddedToCart }) => {
    return (
        <ul className='book-list'>
            {
                books.map( book => {
                    return <li key={book.id}>
                        <BookListItem
                            onAddedToCart={() => onAddedToCart(book.id)}
                            book={book}/></li>
                })
            }
        </ul>
    )
};

class BookListContainer extends Component {

    componentDidMount() {
       this.props.fetchBooks();
    }

    render() {
        const { books, loading, error, onAddedToCart } = this.props;

        if (loading) {
            return <Spinner/>
        }

        if (error) {
            return <ErrorIndicator/>
        }

        return <BookList
            onAddedToCart = { onAddedToCart }
            books={ books }/>
    }
}

const mapStateToProps = ({ bookList: { books, loading, error }}) => {
    return {books, loading, error}
};

const mapDispatchToProps = ( dispatch, ownProps ) => {
    const {bookStoreService} = ownProps;
    return bindActionCreators({
        fetchBooks: fetchBooks(bookStoreService),
        onAddedToCart:bookAddedToCart
    }, dispatch)
};

export default compose (
    withBookstoreService(),
    connect(mapStateToProps, mapDispatchToProps)
)(BookListContainer);
