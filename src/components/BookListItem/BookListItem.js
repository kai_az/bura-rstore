import React from "react";
import './BookListItem.css'

const BookListItem = ({ book, onAddedToCart }) => {
    const { title, author, price, coverImage } = book;

    return (
        <div className='book-list-item'>
            <div className="book-cover">
                <img src={coverImage} alt="cover"/>
            </div>
            <div className="book-details">
                <a href="#" className='book-title'><b>Title:</b> {title}</a>
                <div className='book-author'><b>Author:</b> {author}</div>
                <div className='book-price'><b>Price:</b> {`${price} $`}</div>

                <button
                    onClick = {onAddedToCart}
                    className='btn btn-info add-to-cart'>
                    Add to cart
                </button>
            </div>

        </div>
    )
};

export default BookListItem;
