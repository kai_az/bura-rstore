import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';

import App from './components/App';
import ErrorBoundry from "./components/ErrorBoundry/ErrorBoundry";
import BookstoreService from "./services/bookstore-service";
import { BooksStoreServiceProvider } from "./components/BookStoreServiceContext";
import store from "./store";

const bookstoreService = new BookstoreService();

ReactDOM.render(
  <Provider store = { store }>
      <ErrorBoundry>
          <BooksStoreServiceProvider value={bookstoreService}>
              <Router>
                  <App />
              </Router>
          </BooksStoreServiceProvider>
      </ErrorBoundry>
  </Provider>,
  document.getElementById('root')
);

